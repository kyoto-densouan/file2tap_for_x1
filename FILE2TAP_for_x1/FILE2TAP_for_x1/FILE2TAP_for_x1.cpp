// FILE2TAP_for_x1.cpp : Defines the entry point for the console application.
//
// modified for Sharp X1

#include "stdafx.h"

#include <stdio.h>
#include <stdlib.h>
#include <io.h>

#define D_X1_BIT_RATE		( 8000)			// X1テープフォーマット定義

#define D_FIXED_BIT_RATE	(0x45504154)	// "TAPE"
#define	D_TAPE_NAME			("auto converted  ")

#define	D_PRE_BLANK_COUNT	(59993)			// 先行する無音期間の長さ
#define	D_AFTER_BLANK_COUNT	(59993)			// 後につける無音期間の長さ

// TAPファイルのヘッダ
struct TAP_FILE_HEADER_CNK
{
	unsigned long	type;					// 00H : 識別インデックス
	unsigned char	name[16];				// 04H : テープの名前
	unsigned char	reserve[6];				// 15H : リザーブ
	unsigned char	write_protect;			// 1AH : ライトプロテクトノッチ
	unsigned char	write_format_type;		// 1BH : 記録フォーマットの種類
	unsigned long	bit_rate;				// 1CH : サンプリング周波数
	unsigned long	data_size;				// 20H : テープデータのサイズ
	unsigned long	tape_index;				// 24H : テープの位置
}	TAP_FILE_HEADER;

struct FILE_INFO_CNK
{
	unsigned char type;						// 属性
	unsigned char file_name[13];			// ファイル名
	unsigned char ext_name[3];				// 拡張子
	unsigned char password;					// パスワード:0x20
	unsigned char data_size[2];				// サイズ（下位・上位）
	unsigned char start_address[2];			// 読み込みアドレス（下位・上位）
	unsigned char exec_address[2];			// 開始アドレス（下位・上位）
	unsigned char year;						// BCD
	unsigned char month_week;				// 上位4bit 1~12、下位4bit 曜日
	unsigned char day;						// BCD
	unsigned char hour;						// BCD
	unsigned char minuts;					// BCD
	unsigned char reserve[3];				// 0x1d-0x1f
} FILE_INFO;


unsigned char write_data = 0x00;
unsigned char write_mask = 0x80;
int write_bit_counter = 0;
unsigned short bit1_counter = 0;
unsigned long output_counter = 0;
unsigned short write_1bit(FILE * des, unsigned char bit)
{
	unsigned short result = 0;

	output_counter++;				// 出力データ数更新

	if (bit > 0)
	{
		write_data = write_data | write_mask;
	}
	write_mask = write_mask >> 1;

	if (write_mask == 0)
	{
		size_t ret = fwrite(&write_data, 1, 1, des);

		if (ret < 0)
		{
			result = 1;
		}

		write_data = 0x00;
		write_mask = 0x80;
	}

	return result;
}

unsigned short write_flush(FILE * des)
{
	unsigned short result = 0;

	size_t ret = fwrite(&write_data, 1, 1, des);

	if (ret < 0)
	{
		result = 1;
	}
	fflush(des);

	return result;
}

unsigned short write_0(FILE * des, int cnt = 1)
{
	unsigned short result = 0;

	for (int i = 0; i < cnt; i++)
	{
		result = write_1bit(des, 0);
		if (result > 0)
		{
			break;
		}
		result = write_1bit(des, 1);
		if (result > 0)
		{
			break;
		}
	}
	return result;
}

unsigned short write_1(FILE * des, int cnt = 1)
{
	unsigned short result = 0;

	for (int i = 0; i < cnt; i++)
	{
		result = write_1bit(des, 0);
		if (result > 0)
		{
			break;
		}
		result = write_1bit(des, 0);
		if (result > 0)
		{
			break;
		}
		result = write_1bit(des, 1);
		if (result > 0)
		{
			break;
		}
		result = write_1bit(des, 1);
		if (result > 0)
		{
			break;
		}
	}
	return result;
}

unsigned short write_space(FILE * des, int cnt = 1)
{
	unsigned short result = 0;

	for (int i = 0; i < cnt; i++)
	{
		result = write_1bit(des, 0);
		if (result > 0)
		{
			break;
		}
	}
	return result;
}

unsigned short write_byte(FILE * des, unsigned char data, int startbit=1)
{
	unsigned short result = 0;

	//printf("%02X ", data);

	if (startbit > 0)
	{
		// スタートビット
		result = write_1(des);
		if (result > 0)
		{
			return result;
		}
	}

	unsigned char mask = 0x80;
	while (mask > 0)
	{
		if ((data & mask) == 0)
		{
			result = write_0(des);
		}
		else
		{
			bit1_counter++;
			result = write_1(des);
		}
		mask = mask >> 1;

		if (result > 0)
		{
			break;
		}
	}
	return result;
}

unsigned char write_bytes(FILE * des, unsigned char * pdata, int cnt, int startbit=1)
{
	unsigned char result = 0;

	for (int i = 0; i < cnt; i++)
	{
		result = write_byte(des, pdata[i], startbit);
		if (result > 0)
		{
			break;
		}
	}

	return result;
}

unsigned char write_checksum(FILE * des, unsigned short checksum)
{
	unsigned char result = 0;

	unsigned char data[2];
	data[0] = (checksum >> 8) & 0x00ff;
	data[1] = checksum & 0x00ff;

	result = write_bytes(des, data, 2, 1);

	return result;
}

short make_x1tap(unsigned char *file_name, unsigned char *tap_name, unsigned short start_address, unsigned short exec_address)
{
	unsigned long	flng;			// データ長
	unsigned long	freq;			// ビットレート
	unsigned short	size;			// 出力データ長
	unsigned short	lng;
	unsigned short	offset;
	unsigned short	error = 0;
	unsigned char	bit_mask;
	int				magnification = 1;
	FILE *			des = NULL;
	FILE *			src = NULL;

	errno_t result_src = fopen_s(&src, (const char*)file_name, "rb");
	errno_t result_des = fopen_s(&des, (const char*)tap_name, "wb");
	if (src == NULL || des == NULL)
	{
		if (src != NULL) fclose(src);
		if (des != NULL) fclose(des);
		remove((const char*)tap_name);
		return 1;
	}

	TAP_FILE_HEADER.type				= D_FIXED_BIT_RATE;
	memcpy(TAP_FILE_HEADER.name, D_TAPE_NAME, 16);
	for (int i = 0; i < 6; i++)
	{
		TAP_FILE_HEADER.reserve[i]		= 0;
	}
	TAP_FILE_HEADER.write_protect		= 0x10;
	TAP_FILE_HEADER.write_format_type	= 0x01;
	TAP_FILE_HEADER.bit_rate			= D_X1_BIT_RATE;
	TAP_FILE_HEADER.data_size			= 0x00;
	TAP_FILE_HEADER.tape_index			= 0x00;

	//-----

	// ファイル情報の編集
	FILE_INFO.type				= 0x01;	// bin

	for (int i = 0; i < 13; i++)
	{
		FILE_INFO.file_name[i] = 0x20;
	}
	for (int i = 0; i < 3; i++)
	{
		FILE_INFO.ext_name[i] = 0x20;
	}
										// ファイル名の分離
	int	name_count = 0;
	int fetch_count = 0;
	int ext_count = -1;
	unsigned char c;
	while ((c = file_name[name_count]) != 0x00)
	{
		if ((c == '\\') || (c == '/'))
		{
			fetch_count = name_count + 1;
		}
		name_count++;
	}
	//printf("%s\n", &file_name[fetch_count]);
	name_count = fetch_count;
	while ((c = file_name[name_count]) != 0x00)
	{
		if (c == '.')
		{
			ext_count = name_count + 1;
			break;
		}
		name_count++;
	}
	//printf("%s\n", &file_name[ext_count]);
										// ファイル名
	for (int i = 0; i < 13; i++)
	{
		c = file_name[fetch_count];
		if (c == '.')
		{
			break;
		}
		if (c == 0x00)
		{
			break;
		}
		FILE_INFO.file_name[i] = c;
		fetch_count++;
	}
										// 拡張子名
	if (ext_count >= 0)
	{
		for (int i = 0; i < 3; i++)
		{
			c = file_name[ext_count];
			if (c == '.')
			{
				break;
			}
			if (c == 0x00)
			{
				break;
			}
			FILE_INFO.ext_name[i] = c;
			ext_count++;
		}
	}
	FILE_INFO.password = 0x20;	// パスワード
	unsigned short data_size	= _filelength(_fileno(src));
	FILE_INFO.data_size[0]		= data_size & 0x00ff;
	FILE_INFO.data_size[1]		= (data_size >> 8) & 0x00ff;
	FILE_INFO.start_address[0]  = start_address & 0x00ff;
	FILE_INFO.start_address[1]  = (start_address >> 8) & 0x00ff;
	FILE_INFO.exec_address[0]	= exec_address & 0x00ff;
	FILE_INFO.exec_address[1]	= (exec_address >> 8) & 0x00ff;
	FILE_INFO.year				= 0x17;
	FILE_INFO.month_week		= 0x10;
	FILE_INFO.day				= 0x01;
	FILE_INFO.hour				= 0x00;
	FILE_INFO.minuts			= 0x00;
										// リザーブ
	for (int i = 0; i < 3; i++)
	{
		FILE_INFO.reserve[i]	= 0x00;
	}

	//-----------------------------------------------------------------
	// TAPファイルヘッダ出力
	size_t res = fwrite(&TAP_FILE_HEADER, 1, sizeof(TAP_FILE_HEADER), des);
	if (res < 0)
	{
		error = 1;
	}

	//-----------------------------------------------------------------
	// 先行の無音
	if (error == 0)
	{
		error = write_space(des, D_PRE_BLANK_COUNT);
	}
	//-----------------------------------------------------------------
	// 1が1000
	if (error == 0)
	{
		error = write_1(des, 1000);
	}
	// 0が40
	if (error == 0)
	{
		error = write_0(des, 40);
	}
	// 1が40
	if (error == 0)
	{
		error = write_1(des, 40);
	}
	// 1または0が1
	if (error == 0)
	{
		error = write_1(des, 1);
	}
	// ファイル情報32バイト
	bit1_counter = 0;
	if (error == 0)
	{
		error = write_bytes(des, (unsigned char *)&FILE_INFO, sizeof(FILE_INFO));
	}
	// ファイル情報チェックサム
	if (error == 0)
	{
		//printf("\ncheck sum: %04x\n", bit1_counter);
		error = write_checksum(des, bit1_counter);
	}
	// 1または0が1
	if (error == 0)
	{
		error = write_1(des, 1);
	}
	//-----------------------------------------------------------------
	// 1が3000
	if (error == 0)
	{
		error = write_1(des, 3000);
	}
	// 0が20
	if (error == 0)
	{
		error = write_0(des, 20);
	}
	// 1が20
	if (error == 0)
	{
		error = write_1(des, 20);
	}
	// 1または0が1
	if (error == 0)
		error = write_1(des, 1);

	// データ本体
	bit1_counter = 0;

	if (error == 0)
	{
		unsigned char read_data;
		while (fread(&read_data, 1, 1, src) > 0)
		{
			error = write_byte(des, read_data);
			if (error != 0)
			{
				break;
			}
		}
	}

	// ファイル本体チェックサム
	if (error == 0)
	{
		//printf("\ncheck sum: %04x\n", bit1_counter);
		error = write_checksum(des, bit1_counter);
	}
	// 1または0が1
	if (error == 0)
	{
		error = write_1(des, 1);
	}
	//-----------------------------------------------------------------
	// 後の無音
	if (error == 0)
	{
		error = write_space(des, D_AFTER_BLANK_COUNT);
	}
	if (error == 0)
	{
		error = write_flush(des);
	}
	//-----------------------------------------------------------------
	// TAPファイルヘッダの更新
	if (error == 0)
	{
		printf("output data size: %d\n", output_counter);
		TAP_FILE_HEADER.data_size = output_counter;
		res = fseek(des, 0, SEEK_SET);
		if (res < 0)
		{
			error = 1;
		}
	}

	//-----------------------------------------------------------------
	// TAPファイルヘッダ出力
	if (error == 0)
	{
		res = fwrite(&TAP_FILE_HEADER, 1, sizeof(TAP_FILE_HEADER), des);
		if (res < 0)
		{
			error = 1;
		}
	}

	//-----------------------------------------------------------------

	fclose(src);
	fclose(des);
	if (error)
	{
		remove((const char*)tap_name);
	}

	return error;
}

void main(int argc, unsigned char *argv[])
{
	unsigned char *src = NULL;
	unsigned char *des = NULL;
	short i;
	short err = 0;

	unsigned short start_address = 0x0000;
	unsigned short exec_address = 0x0000;

	for (i = 1; i<argc && !err; i++)
	{
		switch (i){
		case 1:
		case 2:
			if ((argv[i][0] == '/') || (argv[i][0] == '-'))
			{
				err = 1;
			}
			else
			{
				if (src == NULL)
				{
					src = argv[i];
				}
				else if (des == NULL)
				{
					des = argv[i];
				}
				else
				{
					err = 1;
				}
			}
			break;
		case 3:
			start_address = strtol((const char*)argv[i], NULL,0);
			break;
		case 4:
			exec_address = strtol((const char*)argv[i], NULL,0);
			break;
		default:
			break;
		}
	}
	if (src == NULL || des == NULL || err)
	{
		printf("TAP2WAV_for_x1 (Ver.0.1)\n");
		printf("  X1で使用するファイルからTAPへコンバートします\n");
		printf("  TAP2WAV_for_x1 [元ファイル] [先ファイル.TAP] [開始アドレス] [実行アドレス]\n");
		exit(1);
	}

	if (make_x1tap(src, des, start_address, exec_address))
	{
		printf("Error!\n");
	}
	else
	{
		printf("Ok.\n");
	}
}
